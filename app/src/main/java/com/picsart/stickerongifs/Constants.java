package com.picsart.stickerongifs;

import android.content.Context;
import android.os.Environment;

import java.io.File;

/**
 * Created by Tigran on 1/28/2017.
 */

public class Constants {
    public static final int STICKER_WIDTH = 250;
    public static final int STICKER_HEIGHT = 250;

    public static final int SLIDE_DURATION = 2 * 1000; // 2 seconds
    public static final int SLIDE_CHANGE_TIME = 2 * 100; // Time of 2 slides change animation
    public static final int SHOW_FPS = 60; // 60 frames per second
    public static final int SAVE_FPS = 25; // 25 frames per second
    public static final int SHOW_FRAME_TIME = 1000 / SHOW_FPS;
    public static final int SAVE_FRAME_TIME = 1000 / SAVE_FPS;

    public static final String VIDEOS_DIRECTORY_NAME = "Videos";
    public static final String GIFS_DIRECTORY_NAME = "Gifs";

    public static final String getGifsDirectoryPath(Context context) {
        return Environment.getExternalStorageDirectory() + File.separator + context.getPackageName() + File.separator + GIFS_DIRECTORY_NAME;
    }

    public static final String getVideosDirectoryPath(Context context) {
        return Environment.getExternalStorageDirectory() + File.separator + context.getPackageName() + File.separator + VIDEOS_DIRECTORY_NAME;
    }
}