package com.picsart.stickerongifs.utils;

import android.app.Application;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.util.LruCache;
import android.widget.ImageView;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Tigran on 1/27/2017.
 */

public class ImageLoader {

    private static final int CACHE_SIZE = 50 * 1024 * 1024; // 50MB

    ///////////////// Public methods

    public static Bitmap getAssetImageSync(String relativePath) {
        try {
            String assetKey = "asset_" + relativePath;
            synchronized (ImageLoader.getInstance()) {
                Bitmap cached = getInstance().cache.get(assetKey);
                if (cached != null) {
                    return cached;
                }
            }

            InputStream assetStream = getAssetManager().open(relativePath);
            Bitmap assetBmp = BitmapFactory.decodeStream(assetStream);
            synchronized (ImageLoader.getInstance()) {
                getInstance().cache.put(assetKey, assetBmp);
            }

            return assetBmp;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static Bitmap loadBitmapSync(Uri uri, int fitWidth, int fitHeight) {

        Bitmap cached = getInstance().getFromCache(uri.getPath(), fitWidth, fitHeight);
        if (cached != null) {
            return cached;
        }

        int actualFitWidth = fitWidth;
        int actualFitHeight = fitHeight;

        int rotation = getOrientation(uri.getPath());
        if (rotation % 180 != 0) { // Is rotated, swap
            actualFitHeight = fitWidth;
            actualFitWidth = fitHeight;
        }

        final BitmapFactory.Options scaledOptions = getOptimalLoadOptions(uri, actualFitWidth, actualFitHeight);
        if (scaledOptions == null)
            return null;

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = scaledOptions.inSampleSize;

        Bitmap smallerBitmap = BitmapFactory.decodeFile(uri.getPath(), options);
        if (smallerBitmap == null)
            return null;

        Bitmap rotated = rotateBitmapifNeeded(smallerBitmap, rotation);
        if (rotated != null) {
            smallerBitmap.recycle();
            smallerBitmap = rotated;
        }
        getInstance().putInCache(uri.getPath(), smallerBitmap, fitWidth, fitHeight);
        return smallerBitmap;
    }


    ///////////////////////// Helper methods

    private static BitmapFactory.Options getOptimalLoadOptions(Uri uri, int fitWidth, int fitHeight) {
        if (fitWidth <= 0 || fitHeight <= 0)
            return null;

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(uri.getPath(), options);

        int scale = 1;

        while (options.outWidth / 2 >= fitWidth && options.outHeight / 2 >= fitHeight) {
            options.outWidth /= 2;
            options.outHeight /= 2;
            scale *= 2;
        }

        options.inSampleSize = scale;

        return options;
    }

    private static Bitmap rotateBitmapifNeeded(Bitmap source, int angle) {
        if (angle == 0)
            return null;

        Matrix matrix = new Matrix();
        matrix.postRotate(angle);

        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    private static int getOrientation(String path) {
        try {
            File f = new File(path);
            ExifInterface exif = new ExifInterface(f.getPath());
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            int angle = 0;

            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                angle = 90;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                angle = 180;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                angle = 270;
            }

            return angle;
        } catch (IOException | OutOfMemoryError e) {
            e.printStackTrace();
        }

        return 0;
    }

    private static Context context;
    private static AssetManager assetManager;
    private static ImageLoader instance;

    public static void init(Application context) {
        ImageLoader.context = context;
    }

    private static AssetManager getAssetManager() {
        if (assetManager == null)
            assetManager = context.getAssets();
        return assetManager;
    }

    public static ImageLoader getInstance() {
        if (instance == null) {
            instance = new ImageLoader();
        }

        return instance;
    }

    private Map<ImageView, AsyncTask> imageDisplayersMap;
    private LruCache<String, Bitmap> cache;

    public ImageLoader() {
        cache = new LruCache<String, Bitmap>(CACHE_SIZE) {
            @Override
            protected int sizeOf(String key, Bitmap value) {
                return value.getByteCount();
            }
        };

        imageDisplayersMap = new HashMap<>();
    }

    public void displayImage(final Uri uri, final ImageView imageView, int fitSize) {
        displayImage(uri, imageView, fitSize, fitSize);
    }

    public void displayImage(final Uri uri, final ImageView imageView, final int fitWidth, final int fitHeight) {
        if (imageView == null)
            return;

        Bitmap cachedImage = getFromCache(uri.getPath(), fitWidth, fitHeight);
        if (cachedImage != null) {
            imageDisplayersMap.remove(imageView);
            imageView.setImageBitmap(cachedImage);
            return;
        }

        imageView.setImageDrawable(null);

        AsyncTask displayerTask = new AsyncTask<Integer, Void, Bitmap>() {

            @Override
            protected Bitmap doInBackground(Integer... fitDimens) {
                return loadBitmapSync(uri, fitWidth, fitHeight);
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                if (bitmap != null && imageDisplayersMap.get(imageView) == this) {
                    imageDisplayersMap.remove(imageView);
                    imageView.setImageBitmap(bitmap);
                }
            }
        }.execute(fitWidth, fitHeight);

        imageDisplayersMap.put(imageView, displayerTask);
    }

    // Image on disc can be rotated so we pass width and height of raw file.
    // If we get width and height of bitmap it can be different from what we get from getOptimalLoadOptions method
    private synchronized void putInCache(String path, Bitmap image, int width, int height) {
        if (width == 0 || height == 0 || path == null)
            return;

        cache.put(path + "_" + width + "x" + height, image);
    }

    private synchronized Bitmap getFromCache(String path, int width, int height) {
        return cache.get(path + "_" + width + "x" + height);
    }
}
