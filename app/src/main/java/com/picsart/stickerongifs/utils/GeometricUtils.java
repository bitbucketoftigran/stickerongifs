package com.picsart.stickerongifs.utils;

import android.graphics.PointF;


/**
 * Created by Tigran on 1/31/2017.
 */

public class GeometricUtils {
    public static float getDistance(PointF p0, PointF p1) {
        return (float) Math.hypot(p0.x - p1.x, p0.y - p1.y);
    }

    public static double getAngle(PointF point, PointF pivot) {
        return Math.atan2(point.y - pivot.y, point.x - pivot.x);
    }
}
