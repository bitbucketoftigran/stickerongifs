package com.picsart.stickerongifs.utils;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Tigran on 1/20/2016.
 */
public class Toaster {

    public static final int LENGTH_SHORT = Toast.LENGTH_SHORT;
    public static final int LENGTH_LONG = Toast.LENGTH_LONG;

    private static Toast toast;
    private static Context context;

    public static void init(Context context) {
        Toaster.context = context;
    }

    public static void showToast(String text, int duration) {
        if (toast != null)
            toast.cancel();

        toast = Toast.makeText(context, text, duration);
        toast.show();
    }

    public static void showToast(int textResId, int duration) {
        showToast(context.getString(textResId), duration);
    }
}
