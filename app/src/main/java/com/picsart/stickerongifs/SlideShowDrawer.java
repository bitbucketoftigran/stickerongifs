package com.picsart.stickerongifs;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.picsart.stickerongifs.model.BackgroundComponent;
import com.picsart.stickerongifs.model.BackgroundImage;
import com.picsart.stickerongifs.model.Frame;
import com.picsart.stickerongifs.model.Sticker;
import com.picsart.stickerongifs.model.StickerComponent;
import com.picsart.stickerongifs.ui.misc.TimelineItemTouchListener;
import com.picsart.stickerongifs.utils.ImageLoader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Tigran on 1/29/2017.
 */

public class SlideShowDrawer implements TimelineItemTouchListener {

    // This field will always contain information about the last frame drawn.
    private Frame lastFrame;
    private Thread drawingThread;
    private int surfaceWidth;
    private int surfaceHeight;
    private SurfaceHolder surfaceHolder;

    private List<Uri> images;
    private int currentSlideNumber;
    private List<BackgroundImage> backgrounds;
    private List<Sticker> stickers;

    private Runnable drawStartCallback;

    // Need to remember count of slides which have been removed from slideshow
    private long slideShowStartTime;

    public SlideShowDrawer(@NonNull final SurfaceView surfaceView) {

        this.images = new ArrayList<>();

        surfaceHolder = surfaceView.getHolder();
        backgrounds = new ArrayList<>();
        stickers = new ArrayList<>();
        currentSlideNumber = 0;

        surfaceWidth = -1;
        surfaceHeight = -1;
    }

    public int getSlideCount() {
        return images.size();
    }

    public Frame getLastFrame() {
        return lastFrame;
    }

    /**
     * Sets the callback which will be called when slideshow is started
     *
     * @param drawStartCallback
     */
    public void setDrawStartCallback(Runnable drawStartCallback) {
        this.drawStartCallback = drawStartCallback;
    }

    public synchronized void addSticker(Sticker sticker) {
        sticker.init(surfaceWidth, surfaceHeight, Constants.STICKER_WIDTH, Constants.STICKER_HEIGHT);
        this.stickers.add(sticker);
    }

    public synchronized void removeSticker(Sticker sticker) {
        this.stickers.remove(sticker);
    }

    public void startSlideShowWhenSurfaceIsReady(@NonNull List<Uri> images) {
        this.images.clear();
        this.images.addAll(images);
        if (!surfaceHolder.isCreating() && backgrounds.size() > 0) {
            startSlideShow();
        } else {
            surfaceHolder.addCallback(new SurfaceHolder.Callback() {
                @Override
                public void surfaceCreated(SurfaceHolder holder) {

                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... params) {
                            surfaceWidth = surfaceHolder.getSurfaceFrame().width();
                            surfaceHeight = surfaceHolder.getSurfaceFrame().height();

                            // Load first background image and
                            Uri firstImageUri = SlideShowDrawer.this.images.get(0);
                            Bitmap firstImage = ImageLoader.loadBitmapSync(firstImageUri, surfaceWidth, surfaceHeight);
                            backgrounds.clear();
                            BackgroundImage firstBack = new BackgroundImage(firstImage, firstImageUri.getPath());
                            firstBack.init(surfaceWidth, surfaceHeight, surfaceWidth, surfaceHeight);
                            backgrounds.add(firstBack);
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            if (!surfaceHolder.isCreating() && backgrounds.size() > 0)
                                startSlideShow();
                        }
                    }.execute();
                }

                @Override
                public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

                }

                @Override
                public void surfaceDestroyed(SurfaceHolder holder) {

                }
            });
        }
    }

    private synchronized void startSlideShow() {
        if (drawingThread != null && drawingThread.isAlive())
            return;

        if (drawStartCallback != null)
            drawStartCallback.run();
        this.stickers.clear();

        drawingThread = new Thread(new Runnable() {
            @Override
            public void run() {
                // Pretend it's the last slideShowDrawer showing now
                currentSlideNumber = images.size() - 1;
                slideShowStartTime = System.currentTimeMillis() - Constants.SLIDE_DURATION * images.size();
                backgrounds.add(0, null);
                while (drawingThread == Thread.currentThread()) {

                    try {
                        long frameStartMillis = System.currentTimeMillis();
                        long timeDiffInSlideShow = frameStartMillis - slideShowStartTime;
                        // Prepare next slideShowDrawer's image if when slideShowDrawer is changed
                        if (timeDiffInSlideShow / Constants.SLIDE_DURATION > currentSlideNumber) { // First frame of this slideShowDrawer
                            int slideShowDuration = Constants.SLIDE_DURATION * images.size();
                            if (timeDiffInSlideShow >= slideShowDuration) {
                                slideShowStartTime += slideShowDuration;
                                timeDiffInSlideShow %= slideShowDuration;
                            }
                            changeSlide();
                        }

                        drawFrame((int) timeDiffInSlideShow);
                        if (drawingThread != Thread.currentThread()) {
                            return;
                        }

                        long remainingTimeForNextFrame = Constants.SHOW_FRAME_TIME - System.currentTimeMillis() + frameStartMillis;
                        if (remainingTimeForNextFrame > 0)
                            Thread.sleep(remainingTimeForNextFrame);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        return;
                    }
                }
            }
        });

        drawingThread.start();
    }

    public synchronized void stopSlideShow() {
        if (drawingThread != null && drawingThread.isAlive()) {
            // Interrupting drawingThread doesn't matter, because lockCanvas() method clears interrupt flag.
            drawingThread = null;
            backgrounds.clear();
        }
    }

    private void drawFrame(int slideShowOffsetTime) {
        if (drawingThread != Thread.currentThread()) {
            return;
        }
        Canvas canvas = surfaceHolder.lockCanvas();
        if (canvas == null)
            return;

        List<BackgroundImage> backgrounds;
        List<Sticker> stickers;

        // This method is not synchronized so ensure background and stickers lists won't be changed during drawing process
        synchronized (this) {
            backgrounds = new ArrayList<>(this.backgrounds);
            stickers = new ArrayList<>(this.stickers);
        }

        if (backgrounds.size() == 0) {
            surfaceHolder.unlockCanvasAndPost(canvas);
            return;
        }

        Frame frame = new Frame();

        int slideOffsetTime = slideShowOffsetTime % Constants.SLIDE_DURATION;

        canvas.drawColor(Color.TRANSPARENT);

        if (slideOffsetTime < Constants.SLIDE_DURATION - Constants.SLIDE_CHANGE_TIME || backgrounds.size() == 1) { // Time to show only one background or second background is missing
            BackgroundImage image = backgrounds.get(0);
            image.setAlpha(255);
            BackgroundComponent backImage = image.drawOnCanvas(canvas);
            frame.addComponent(backImage);

        } else if (backgrounds.size() == 2) { // Time to change background. Show current and next with appropriate alphas
            int alphaOfCurrentImage = 255 * (slideOffsetTime - Constants.SLIDE_DURATION + Constants.SLIDE_CHANGE_TIME) / Constants.SLIDE_CHANGE_TIME;

            BackgroundImage firstImage = backgrounds.get(0);
            firstImage.setAlpha(255 - alphaOfCurrentImage);
            BackgroundComponent backImageFirst = firstImage.drawOnCanvas(canvas);
            frame.addComponent(backImageFirst);

            BackgroundImage secondImage = backgrounds.get(1);
            secondImage.setAlpha(alphaOfCurrentImage);
            BackgroundComponent backImageSecond = secondImage.drawOnCanvas(canvas);
            frame.addComponent(backImageSecond);
        }

        for (Sticker sticker : stickers) {
            StickerComponent stickerComponent = sticker.drawOnCanvas(canvas);
            frame.addComponent(stickerComponent);
        }

        lastFrame = frame;

        surfaceHolder.unlockCanvasAndPost(canvas);
    }

    /**
     * This methods changes current slideShowDrawer image and loads next slideShowDrawer's image in a separate thread.
     */
    private synchronized void changeSlide() {
        backgrounds.remove(0);
        currentSlideNumber++;
        if (currentSlideNumber >= images.size())
            currentSlideNumber = 0;

        new Thread(new Runnable() {
            @Override
            public void run() {
                Uri nextImageUri = images.get((currentSlideNumber + 1) % images.size());
                Bitmap nextImage = ImageLoader.loadBitmapSync(nextImageUri, surfaceWidth, surfaceHeight);
                synchronized (SlideShowDrawer.this) {
                    BackgroundImage nextBack = new BackgroundImage(nextImage, nextImageUri.getPath());
                    nextBack.init(surfaceWidth, surfaceHeight, surfaceWidth, surfaceHeight);
                    backgrounds.add(nextBack);
                }
            }
        }).start();
    }

    @Override
    public synchronized void onItemDismiss(int position) {
        if (position <= currentSlideNumber) {
            currentSlideNumber--;
            slideShowStartTime += Constants.SLIDE_DURATION;
        }
        images.remove(position);
    }

    @Override
    public synchronized boolean onItemMove(int fromPosition, int toPosition) {
        Collections.swap(images, fromPosition, toPosition);
        return false;
    }
}