package com.picsart.stickerongifs.ui.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageView;

import com.picsart.stickerongifs.R;
import com.picsart.stickerongifs.SlideShowDrawer;
import com.picsart.stickerongifs.SlideShowRecorder;
import com.picsart.stickerongifs.model.Sticker;
import com.picsart.stickerongifs.ui.adapter.TimelineAdapter;
import com.picsart.stickerongifs.ui.misc.TimeLineItemTouchDecoration;
import com.picsart.stickerongifs.utils.ImageLoader;
import com.picsart.stickerongifs.utils.PermissionUtils;

import java.util.ArrayList;
import java.util.List;

public class EditorActivity extends MenuNextActivity {

    private ArrayList<Uri> slideShowImages;
    private SurfaceView slideShowSurface;
    private TimelineAdapter timelineAdapter;

    private SlideShowDrawer slideShowDrawer;
    private SlideShowRecorder slideShowRecorder;
    private List<Sticker> stickers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // Ignore images in intent if savedInstanceState is not null
        // New images will be added in onNewIntent method
        if (savedInstanceState != null)
            slideShowImages = savedInstanceState.getParcelableArrayList("images");
        else if (slideShowImages == null)
            slideShowImages = getIntent().getParcelableArrayListExtra("images");

        stickers = new ArrayList<>();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);
        initActionbar();
        initTimeline();
        initStickers();

        slideShowSurface = (SurfaceView) findViewById(R.id.slide_show_surface);
        slideShowSurface.setZOrderOnTop(true);
        slideShowDrawer = new SlideShowDrawer(slideShowSurface);
        slideShowRecorder = new SlideShowRecorder(slideShowDrawer);

        slideShowSurface.setOnTouchListener(new View.OnTouchListener() {

            private SparseArray<Sticker> pointerStickers = new SparseArray<>();

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getActionMasked();

                if (action == MotionEvent.ACTION_DOWN || action == MotionEvent.ACTION_POINTER_DOWN) {
                    int actionIndex = event.getActionIndex();
                    int pointerId = event.getPointerId(actionIndex);
                    float x = event.getX(actionIndex);
                    float y = event.getY(actionIndex);
                    for (Sticker sticker : stickers) {
                        boolean accepted = sticker.handleTouchDown(pointerId, x, y);
                        if (accepted) {
                            pointerStickers.put(pointerId, sticker);
                            return true;
                        }
                    }

                    return true;
                }

                if (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_POINTER_UP) {
                    int actionIndex = event.getActionIndex();
                    int pointerId = event.getPointerId(actionIndex);

                    float x = event.getX(actionIndex);
                    float y = event.getY(actionIndex);

                    if (pointerStickers.indexOfKey(pointerId) >= 0) {
                        Sticker sticker = pointerStickers.get(pointerId);

                        boolean deleted = sticker.handleTouchUp(pointerId, x, y);

                        pointerStickers.remove(pointerId);
                        if (deleted) {
                            slideShowDrawer.removeSticker(sticker);
                            stickers.remove(sticker);
                        }
                    }

                    return true;
                }

                if (action == MotionEvent.ACTION_MOVE) {
                    for (int i = 0; i < event.getPointerCount(); i++) {
                        int pointerId = event.getPointerId(i);
                        if (pointerStickers.indexOfKey(pointerId) < 0)
                            continue;

                        Sticker sticker = pointerStickers.get(pointerId);
                        float x = event.getX(i);
                        float y = event.getY(i);
                        sticker.handleTouchMove(pointerId, x, y);
                    }
                }

                return true;
            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        ArrayList<Uri> newImages = intent.getParcelableArrayListExtra("images");
        if (newImages != null) {
            // Remove this data
            getIntent().putParcelableArrayListExtra("images", null);
            this.slideShowImages.addAll(newImages);
            this.timelineAdapter.addImages(newImages);
        }

        setIntent(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        slideShowRecorder.stopRecording();
        slideShowDrawer.stopSlideShow();
    }

    @Override
    protected void onResume() {
        super.onResume();

        boolean granted = PermissionUtils.checkStoragePermission(this, new Runnable() {
            @Override
            public void run() {
                moveTaskToBack(true);
            }
        });

        if (granted) {
            slideShowDrawer.startSlideShowWhenSurfaceIsReady(slideShowImages);
            slideShowDrawer.setDrawStartCallback(new Runnable() {
                @Override
                public void run() {
                    slideShowRecorder.startRecording();
                }
            });
        }
    }

    private void initTimeline() {
        RecyclerView timelineList = (RecyclerView) findViewById(R.id.timeline);

        final int timelineItemsGap = getResources().getDimensionPixelSize(R.dimen.gap_tiny);

        TimeLineItemTouchDecoration decoration = new TimeLineItemTouchDecoration(timelineItemsGap, new ItemTouchHelper.Callback() {
            @Override
            public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                int swipeFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
                int dragFlags = ItemTouchHelper.START | ItemTouchHelper.END;
                return makeMovementFlags(dragFlags, swipeFlags);
            }

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                int moveStart = viewHolder.getAdapterPosition();
                int moveTarget = target.getAdapterPosition();
                timelineAdapter.onItemMove(moveStart, moveTarget);
                slideShowDrawer.onItemMove(moveStart, moveTarget);
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int itemIndex = viewHolder.getAdapterPosition();
                timelineAdapter.onItemDismiss(itemIndex);
                slideShowDrawer.onItemDismiss(itemIndex);
            }

            @Override
            public boolean isLongPressDragEnabled() {
                return true;
            }

            @Override
            public boolean isItemViewSwipeEnabled() {
                return true;
            }
        });

        decoration.attachToRecyclerView(timelineList);

        LinearLayoutManager timelineLayoutManager = new LinearLayoutManager(this);
        timelineLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        timelineList.setLayoutManager(timelineLayoutManager);

        timelineAdapter = new TimelineAdapter(this);
        timelineList.setAdapter(timelineAdapter);

        timelineAdapter.addImages(slideShowImages);
    }

    private void initStickers() {

        //TODO make this better
        ImageView sticker0 = (ImageView) findViewById(R.id.sticker_0);
        ImageView sticker1 = (ImageView) findViewById(R.id.sticker_1);
        ImageView sticker2 = (ImageView) findViewById(R.id.sticker_2);

        final String path0 = "stickers/dart_vader.png";
        final String path1 = "stickers/death_star.png";
        final String path2 = "stickers/yoda.png";

        final Bitmap asset0 = ImageLoader.getAssetImageSync(path0);
        final Bitmap asset1 = ImageLoader.getAssetImageSync(path1);
        final Bitmap asset2 = ImageLoader.getAssetImageSync(path2);

        sticker0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addSticker(asset0, path0);
            }
        });

        sticker1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addSticker(asset1, path1);
            }
        });

        sticker2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addSticker(asset2, path2);
            }
        });

        sticker0.setImageBitmap(asset0);
        sticker1.setImageBitmap(asset1);
        sticker2.setImageBitmap(asset2);
    }

    private void addSticker(Bitmap image, String path) {

        Sticker sticker = new Sticker(image, path, getResources().getColor(R.color.colorPrimary));

        this.stickers.add(0, sticker);
        this.slideShowDrawer.addSticker(sticker);
    }

    @Override
    protected boolean onMenuNextClicked() {
        this.slideShowDrawer.stopSlideShow();
        this.slideShowRecorder.stopRecording();
        Intent intent = new Intent(this, SaveActivity.class);
        intent.putExtra("frame_width", slideShowSurface.getHolder().getSurfaceFrame().width());
        intent.putExtra("frame_height", slideShowSurface.getHolder().getSurfaceFrame().height());
        intent.putParcelableArrayListExtra("frames", this.slideShowRecorder.getFrames());
        startActivity(intent);
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList("images", slideShowImages);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected int getNavigationHomeIcon() {
        return android.R.drawable.ic_menu_close_clear_cancel;
    }
}