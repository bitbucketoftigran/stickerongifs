package com.picsart.stickerongifs.ui.activity;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;

import com.picsart.stickerongifs.R;
import com.picsart.stickerongifs.encoder.EncodeType;
import com.picsart.stickerongifs.encoder.EncoderIntentService;
import com.picsart.stickerongifs.model.Frame;
import com.picsart.stickerongifs.utils.PermissionUtils;

import java.util.ArrayList;
import java.util.UUID;

public class SaveActivity extends ActionbarActivity {

    private ArrayList<Frame> frames;
    private int frameWidth;
    private int frameHeight;

    private String broadcastName;
    private BroadcastReceiver updatesReceiver;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        frameWidth = getIntent().getIntExtra("frame_width", -1);
        frameHeight = getIntent().getIntExtra("frame_height", -1);
        frames = getIntent().getParcelableArrayListExtra("frames");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save);
        initActionbar();
    }

    public void clickSaveGif(View v) {
        encodeAs(EncodeType.GIF);
    }

    public void clickSaveVideo(View v) {
        encodeAs(EncodeType.MP4);
    }

    private void encodeAs(EncodeType type) {

        boolean granted = PermissionUtils.checkStoragePermission(this, null);

        if (!granted) {
            return;
        }

        broadcastName = UUID.randomUUID().toString();
        Intent encodeIntent = new Intent(this, EncoderIntentService.class);
        encodeIntent.putParcelableArrayListExtra("frames", frames);
        encodeIntent.putExtra("frame_width", frameWidth);
        encodeIntent.putExtra("frame_height", frameHeight);
        encodeIntent.putExtra("broadcast_name", broadcastName);
        encodeIntent.putExtra("encode_type", type);

        startService(encodeIntent);

        IntentFilter progressFilter = new IntentFilter();
        progressFilter.addAction(broadcastName + "_progress");
        progressFilter.addAction(broadcastName + "_finish");

        updatesReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (action.equals(broadcastName + "_progress")) {
                    int index = intent.getIntExtra("frame_index", -1);
                    progressDialog.setMessage(getString(R.string.progress_frames, index, frames.size()));
                } else if (action.equals(broadcastName + "_finish")) {
                    unregisterReceiver(this);
                    broadcastName = null;
                    updatesReceiver = null;
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            }
        };
        registerReceiver(updatesReceiver, progressFilter);

        progressDialog = ProgressDialog.show(this, null, getString(R.string.progress_frames, 0, frames.size()));
    }

    @Override
    protected void onDestroy() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
        if (updatesReceiver != null)
            unregisterReceiver(updatesReceiver);
        super.onDestroy();
    }

    @Override
    protected int getNavigationHomeIcon() {
        return android.R.drawable.ic_menu_close_clear_cancel;
    }
}