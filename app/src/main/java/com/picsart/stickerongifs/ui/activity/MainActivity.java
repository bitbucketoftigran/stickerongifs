package com.picsart.stickerongifs.ui.activity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.picsart.stickerongifs.Constants;
import com.picsart.stickerongifs.R;
import com.picsart.stickerongifs.ui.adapter.SelectableImagesAdapter;
import com.picsart.stickerongifs.utils.PermissionUtils;
import com.picsart.stickerongifs.utils.Toaster;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends MenuNextActivity {

    private SelectableImagesAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initActionbar();

        RecyclerView imagesGrid = (RecyclerView) findViewById(R.id.images_grid);
        RecyclerView.LayoutManager gridLayoutManager = new GridLayoutManager(this, 3);

        final int gap = getResources().getDimensionPixelSize(R.dimen.gap_default);
        imagesGrid.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                outRect.left = gap / 2;
                outRect.right = gap / 2;
                outRect.bottom = gap;
            }
        });
        imagesGrid.setLayoutManager(gridLayoutManager);
        adapter = new SelectableImagesAdapter(this);
        imagesGrid.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        boolean granted = PermissionUtils.checkStoragePermission(this, new Runnable() {
            @Override
            public void run() {
                moveTaskToBack(true);
            }
        });

        if (granted) {
            populateWithAllImages();
            populateWithSavedGifs();
        }
    }

    @Override
    protected boolean onMenuNextClicked() {
        ArrayList<Uri> images = adapter.getSelectedImages();
        if (images.size() == 0) {
            Toaster.showToast(R.string.toast_select_images, Toaster.LENGTH_SHORT);
            return true;
        }
        Intent intent = new Intent(this, EditorActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putParcelableArrayListExtra("images", images);
        startActivity(intent);
        return true;
    }

    private void populateWithSavedGifs() {

        File gifsPath = new File(Constants.getGifsDirectoryPath(this));
        if (!gifsPath.exists() || !gifsPath.isDirectory())
            return;

        File[] savedGifs = new File(Constants.getGifsDirectoryPath(this)).listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".gif");
            }
        });

        List<String> savedGifsFullPaths = new ArrayList<>(savedGifs.length);
        for (File file : savedGifs)
            savedGifsFullPaths.add(file.getAbsolutePath());
        adapter.setGifs(savedGifsFullPaths);
    }

    private void populateWithAllImages() {
        String[] columns = new String[]{MediaStore.Images.Media.DATA}; // Only uri is needed

        Cursor cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns, null, null, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                List<Uri> uris = new ArrayList<>();
                do {
                    String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
                    uris.add(Uri.parse(path));
                } while (cursor.moveToNext());
                adapter.setImages(uris);
            }
            cursor.close();
        }
    }
}
