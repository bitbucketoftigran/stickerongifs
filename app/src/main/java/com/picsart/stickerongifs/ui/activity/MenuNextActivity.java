package com.picsart.stickerongifs.ui.activity;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.picsart.stickerongifs.R;

/**
 * Created by Tigran on 1/28/2017.
 */

public abstract class MenuNextActivity extends ActionbarActivity {

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_next, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_next) {
            return onMenuNextClicked();
        }

        return super.onOptionsItemSelected(item);
    }

    protected abstract boolean onMenuNextClicked();
}
