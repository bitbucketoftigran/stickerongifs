package com.picsart.stickerongifs.ui.misc;

/**
 * Created by Tigran on 2/3/2017.
 */

public interface TimelineItemTouchListener {
    public void onItemDismiss(int position);

    public boolean onItemMove(int fromPosition, int toPosition);
}
