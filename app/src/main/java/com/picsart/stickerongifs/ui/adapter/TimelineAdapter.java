package com.picsart.stickerongifs.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.picsart.stickerongifs.R;
import com.picsart.stickerongifs.ui.activity.MainActivity;
import com.picsart.stickerongifs.ui.misc.TimelineItemTouchListener;
import com.picsart.stickerongifs.utils.ImageLoader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Tigran on 1/28/2017.
 */

public class TimelineAdapter extends RecyclerView.Adapter<TimelineAdapter.TimelineItemViewHolder> implements TimelineItemTouchListener {

    private List<Uri> imageUris;
    private LayoutInflater inflater;

    public TimelineAdapter(Context context) {
        this.inflater = LayoutInflater.from(context);
        imageUris = new ArrayList<>();
    }

    public void addImages(List<Uri> imageUris) {
        if (imageUris == null)
            return;

        int currentCount = this.imageUris.size();

        for (Uri uri : imageUris)
            if (uri != null)
                this.imageUris.add(uri);

        notifyItemRangeInserted(currentCount, this.imageUris.size() - currentCount);
    }

    @Override
    public void onItemDismiss(int position) {
        imageUris.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(imageUris, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(imageUris, i, i - 1);
            }
        }
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }

    @Override
    public TimelineItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TimelineItemViewHolder(inflater.inflate(R.layout.item_timeline, parent, false));
    }

    @Override
    public void onBindViewHolder(TimelineItemViewHolder holder, int position) {
        if (position < imageUris.size()) {
            ImageLoader.getInstance().displayImage(imageUris.get(position), holder.image, 100);
            holder.image.setScaleType(ImageView.ScaleType.CENTER_CROP);
        } else {
            holder.image.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            holder.image.setImageResource(android.R.drawable.ic_menu_add);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //TODO move this to activity
                    v.getContext().startActivity(new Intent(v.getContext(), MainActivity.class));
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return imageUris.size() + 1; // +1 for plus button
    }

    class TimelineItemViewHolder extends RecyclerView.ViewHolder {

        private ImageView image;

        public TimelineItemViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.image);
        }
    }
}
