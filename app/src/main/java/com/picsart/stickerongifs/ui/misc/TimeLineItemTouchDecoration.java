package com.picsart.stickerongifs.ui.misc;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tigran on 2/3/2017.
 */

public class TimeLineItemTouchDecoration extends ItemTouchHelper {

    private int timelineItemsGap;

    /**
     * Creates an ItemTouchHelper that will work with the given Callback.
     * <p>
     * You can attach ItemTouchHelper to a RecyclerView via
     * {@link #attachToRecyclerView(RecyclerView)}. Upon attaching, it will add an item decoration,
     * an onItemTouchListener and a Child attach / detach listener to the RecyclerView.
     *
     * @param timelineItemsGap gap in pixels between timeline items
     */
    public TimeLineItemTouchDecoration(int timelineItemsGap, ItemTouchHelper.Callback callback) {
        super(callback);
        this.timelineItemsGap = timelineItemsGap;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.right = timelineItemsGap;
    }
}
