package com.picsart.stickerongifs.ui.activity;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.picsart.stickerongifs.R;
import com.picsart.stickerongifs.utils.PermissionUtils;

/**
 * Created by Tigran on 1/27/2017.
 */

public abstract class ActionbarActivity extends AppCompatActivity {
    protected final void initActionbar() {
        Toolbar myToolbar = (Toolbar) findViewById(R.id.action_bar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle(null);
        int iconId = getNavigationHomeIcon();
        if (iconId != 0)
            myToolbar.setNavigationIcon(iconId);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected int getNavigationHomeIcon() {
        return 0;
    }
}
