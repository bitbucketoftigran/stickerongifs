package com.picsart.stickerongifs.ui.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Movie;
import android.util.AttributeSet;
import android.view.View;

import com.picsart.stickerongifs.Constants;

/**
 * Created by Tigran on 2/3/2017.
 */

public class GifImageView extends View {

    private String gifFilePath;
    private Movie movie;
    private int currentTime;

    public GifImageView(Context context) {
        super(context);
    }

    public GifImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GifImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setGifFilePath(String gifFilePath) {
        setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        this.gifFilePath = gifFilePath;
        movie = Movie.decodeFile(gifFilePath);
        currentTime = 0;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (movie != null) {
            long drawStartTime = System.currentTimeMillis();
            movie.setTime(currentTime);
            currentTime = (currentTime + Constants.SAVE_FRAME_TIME) % movie.duration();
            canvas.scale((float) this.getMeasuredWidth() / (float) movie.width(), (float) this.getMeasuredHeight() / (float) movie.width());
            movie.draw(canvas, 0, 0);
            long drawTime = System.currentTimeMillis() - drawStartTime;
            if (drawTime >= Constants.SAVE_FRAME_TIME) {
                invalidate();
            } else {
                postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        invalidate();
                    }
                }, Constants.SAVE_FRAME_TIME - drawTime);
            }
        }
    }
}