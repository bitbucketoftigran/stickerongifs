package com.picsart.stickerongifs.ui.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.picsart.stickerongifs.R;
import com.picsart.stickerongifs.model.SelectableImage;
import com.picsart.stickerongifs.ui.view.GifImageView;
import com.picsart.stickerongifs.utils.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tigran on 1/27/2017.
 */

public class SelectableImagesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int VIEW_TYPE_GIF = 0;
    private static final int VIEW_TYPE_IMAGE = 1;

    private List<String> gifs;
    private List<SelectableImage> allImages;
    // Keep all selected images in a separate list so we won't have to iterate through all images and fix indexes every time an item was unselected.
    private List<SelectableImage> selectedImages;

    private Context context;
    private LayoutInflater inflater;

    public SelectableImagesAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        allImages = new ArrayList<>();
        selectedImages = new ArrayList<>();
        gifs = new ArrayList<>();
    }

    public void setGifs(List<String> gifs) {
        this.gifs.clear();
        this.gifs.addAll(gifs);
        this.notifyItemRangeInserted(0, gifs.size());
    }

    public void setImages(List<Uri> imageUris) {
        this.allImages.clear();
        addImages(imageUris);
    }

    public void addImages(List<Uri> imageUris) {
        int currentCount = this.allImages.size();
        for (Uri uri : imageUris)
            if (uri != null)
                this.allImages.add(new SelectableImage(uri));

        notifyItemRangeInserted(currentCount, this.allImages.size() - currentCount); // There can be null Uris in list, so we need to calculate the number of inserted items.
    }

    private void selectItem(SelectableImage item) {
        item.setIndex(selectedImages.size());
        selectedImages.add(item);
    }

    private void unselectItem(SelectableImage item) {
        item.setIndex(-1);
        selectedImages.remove(item);

        int i = 0;
        for (SelectableImage selectedImage : selectedImages)
            selectedImage.setIndex(i++);
    }

    public ArrayList<Uri> getSelectedImages() {
        ArrayList<Uri> selectedImageUris = new ArrayList<>(selectedImages.size()); // Avoid encapsulation breaks
        for (SelectableImage item : selectedImages)
            selectedImageUris.add(item.getSourceUri());

        return selectedImageUris;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return viewType == VIEW_TYPE_GIF ?
                new GifImageViewHolder(inflater.inflate(R.layout.item_gif_image, parent, false)) :
                new SelectableImageViewHolder(inflater.inflate(R.layout.item_selectable_image, parent, false));
    }

    @Override
    public int getItemViewType(int position) {
        return position < gifs.size() ? VIEW_TYPE_GIF : VIEW_TYPE_IMAGE;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case VIEW_TYPE_GIF:
                ((GifImageViewHolder) holder).gifImage.setGifFilePath(gifs.get(position));
                break;
            case VIEW_TYPE_IMAGE:
                populateImageItem((SelectableImageViewHolder) holder, position);
                break;
        }
    }

    private void populateImageItem(SelectableImageViewHolder holder, final int position) {
        int imagePosition = position - gifs.size();
        final SelectableImage item = allImages.get(imagePosition);
        ImageLoader.getInstance().displayImage(item.getSourceUri(), holder.image, 100);

        int displayIndex = item.getDisplayIndex();
        int selectionVisibility = displayIndex == 0 ? View.INVISIBLE : View.VISIBLE;

        holder.indexText.setVisibility(selectionVisibility);
        holder.selectedOverlay.setVisibility(selectionVisibility);

        if (displayIndex > 0)
            holder.indexText.setText(String.valueOf(displayIndex));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int index = item.getIndex();
                if (index == -1) {
                    selectItem(item);
                    notifyItemChanged(position);
                } else {
                    unselectItem(item);
                    notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return gifs.size() + allImages.size();
    }

    class GifImageViewHolder extends RecyclerView.ViewHolder {
        private GifImageView gifImage;

        public GifImageViewHolder(View itemView) {
            super(itemView);
            this.gifImage = (GifImageView) itemView.findViewById(R.id.image);
        }
    }

    class SelectableImageViewHolder extends RecyclerView.ViewHolder {

        private ImageView image;
        private TextView indexText;
        private View selectedOverlay;

        public SelectableImageViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.image);
            indexText = (TextView) itemView.findViewById(R.id.index_text);
            selectedOverlay = itemView.findViewById(R.id.selected_overlay);
        }
    }
}
