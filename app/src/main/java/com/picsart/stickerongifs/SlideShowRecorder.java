package com.picsart.stickerongifs;

import com.picsart.stickerongifs.model.Frame;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Tigran on 2/2/2017.
 */

public class SlideShowRecorder {
    private int lastFrameIndex;
    private ArrayList<Frame> frames;
    public SlideShowDrawer slideShowDrawer;
    private Timer timer;

    public SlideShowRecorder(SlideShowDrawer drawer) {
        this.slideShowDrawer = drawer;
        this.frames = new ArrayList<>();
    }

    public void startRecording() {
        if (slideShowDrawer == null)
            return;

        this.frames.clear();
        this.lastFrameIndex = -1;

        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                int framesCount = slideShowDrawer.getSlideCount() * Constants.SLIDE_DURATION / Constants.SAVE_FPS;

                Frame frame = slideShowDrawer.getLastFrame();
                if (frame == null)
                    return;
                lastFrameIndex++;
                if (lastFrameIndex > framesCount-1) {
                    // Last frame index will be bigger if a background image was removed.
                    // So we need to remove all the frames after framesCount index
                    frames.subList(framesCount, frames.size()).clear();
                    lastFrameIndex = 0;
                }
                if (lastFrameIndex == frames.size())
                    frames.add(frame);
                else
                    frames.set(lastFrameIndex, frame);
            }
        }, 0, Constants.SAVE_FPS);
    }

    public void stopRecording() {
        if (timer != null)
            timer.cancel();
    }

    public ArrayList<Frame> getFrames() {
        return frames;
    }
}
