package com.picsart.stickerongifs;

import android.app.Application;

import com.picsart.stickerongifs.utils.ImageLoader;
import com.picsart.stickerongifs.utils.Toaster;

/**
 * Created by Tigran on 1/27/2017.
 */

public class StickerOnGifsApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Toaster.init(this);
        ImageLoader.init(this);
    }
}
