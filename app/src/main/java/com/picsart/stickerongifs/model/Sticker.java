package com.picsart.stickerongifs.model;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RectF;
import android.util.SparseArray;

import com.picsart.stickerongifs.Constants;
import com.picsart.stickerongifs.utils.GeometricUtils;

import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Created by Tigran on 1/30/2017.
 */

public class Sticker extends DrawableObject {

    private static final int POINTER_MAX_NUMBER = 2;
    private static final int DELETE_SIZE = 80;

    private String assetRelativePath;

    private Paint deleteCirclePaint;
    private Paint deleteXPaint;

    private StickerComponent initialState;
    private StickerComponent currentState;
    private SparseArray<PointF> pointerInitialPositions;
    private SparseArray<PointF> pointerPositions;
    private SortedSet<Integer> additionalPointers;
    private SortedSet<Integer> deletePointers;

    public Sticker(Bitmap bitmap, String assetRelativePath, int deleteColor) {

        super(bitmap);
        this.assetRelativePath = assetRelativePath;

        deleteCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        deleteCirclePaint.setColor(deleteColor);
        deleteXPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        deleteXPaint.setColor(Color.argb(100, 255, 255, 255));
        deleteXPaint.setStrokeWidth(10);

        deleteCirclePaint.setColor(deleteColor);

        pointerInitialPositions = new SparseArray<>(POINTER_MAX_NUMBER);
        pointerPositions = new SparseArray<>(POINTER_MAX_NUMBER);
        additionalPointers = new TreeSet<>();
        deletePointers = new TreeSet<>();
    }

    @Override
    public void init(float surfaceWidth, float surfaceHeight, float width, float height) {
        // Draw in center
        initialState = new StickerComponent(assetRelativePath, (surfaceWidth - Constants.STICKER_WIDTH) / 2, (surfaceHeight - Constants.STICKER_HEIGHT) / 2, width, height);
        currentState = new StickerComponent(initialState);
    }

    /**
     * @param pointerId
     * @param x
     * @param y
     * @return true, if delete button was clicked. False otherwise.
     */
    public synchronized boolean handleTouchUp(int pointerId, float x, float y) {

        if (additionalPointers.contains(pointerId)) {
            additionalPointers.remove(pointerId);
        } else if (deletePointers.contains(pointerId)) {
            float currentCornerX = currentState.getX();
            float currentCornerY = currentState.getY();
            if (x > currentCornerX - DELETE_SIZE / 2 && x < currentCornerX + DELETE_SIZE / 2 && y > currentCornerY - DELETE_SIZE / 2 && y < currentCornerY + DELETE_SIZE / 2) {
                deletePointers.remove(pointerId);
                if (deletePointers.size() == 0) {
                    return true;
                }
            }
        } else if (pointerPositions.indexOfKey(pointerId) >= 0) {

            PointF position = pointerPositions.get(pointerId);
            position.set(x, y);

            initialState.copyFrom(currentState);

            updateInitialState();

            pointerInitialPositions.remove(pointerId);
            pointerPositions.remove(pointerId);
        }

        return false;
    }

    public synchronized boolean handleTouchMove(int pointerId, float x, float y) {
        if (pointerPositions.indexOfKey(pointerId) >= 0) {
            PointF position = pointerPositions.get(pointerId);
            position.set(x, y);
            updateCurrentState();
            return true;
        } else if (deletePointers.contains(pointerId)) {
            // A pointer which was down on delete button was moved. Do nothing.
            return true;
        } else {
            return false;
        }
    }

    public synchronized boolean handleTouchDown(int pointerId, float x, float y) {

        float currentCornerX = currentState.getX();
        float currentCornerY = currentState.getY();
        if (x > currentCornerX - DELETE_SIZE / 2 && x < currentCornerX + DELETE_SIZE / 2 && y > currentCornerY - DELETE_SIZE / 2 && y < currentCornerY + DELETE_SIZE / 2) {
            deletePointers.add(pointerId);
            return true;
        }

        // Check if sticker should handle this event
        if (!currentState.contains(x, y)) {
            // Not my pointer
            return false;
        }

        if (pointerPositions.size() == POINTER_MAX_NUMBER) {
            additionalPointers.add(pointerId);
        } else {
            pointerPositions.put(pointerId, new PointF(x, y));
            pointerInitialPositions.put(pointerId, new PointF(x, y));
        }
        return true;
    }

    private void updateInitialState() {
        // Set initial pointer positions to current.
        // Clear initial pointers list and fill with copies of current list
        pointerInitialPositions.clear();
        for (int i = 0; i < pointerPositions.size(); i++) {
            PointF currentPosition = pointerPositions.valueAt(i);
            pointerInitialPositions.put(pointerPositions.keyAt(i), new PointF(currentPosition.x, currentPosition.y));
        }

        initialState.copyFrom(currentState);
    }

    @Override
    public synchronized StickerComponent drawOnCanvas(Canvas canvas) {
        float pivotX = currentState.getX();
        float pivotY = currentState.getY();
        canvas.rotate((float) Math.toDegrees(currentState.getAngle()), pivotX, pivotY);
        RectF rect = new RectF(pivotX, pivotY, pivotX + currentState.getWidth(), pivotY + currentState.getHeight());
        canvas.drawBitmap(bitmap, null, rect, imagePaint);
        canvas.rotate((float) -Math.toDegrees(currentState.getAngle()), currentState.getX(), currentState.getY());
        canvas.drawCircle(pivotX, pivotY, DELETE_SIZE / 2, deleteCirclePaint);
        float lineSize = DELETE_SIZE * 0.5f;
        canvas.drawLine(pivotX - lineSize / 2, pivotY - lineSize / 2, pivotX + lineSize / 2, pivotY + lineSize / 2, deleteXPaint);
        canvas.drawLine(pivotX + lineSize / 2, pivotY - lineSize / 2, pivotX - lineSize / 2, pivotY + lineSize / 2, deleteXPaint);
        return getCurrentState();
    }

    @Override
    public StickerComponent getCurrentState() {
        return new StickerComponent(currentState);
    }

    private void updateCurrentState() {
        if (pointerPositions.size() == 0) {
            currentState.copyFrom(initialState);
            return;
        }

        PointF initialPointer0 = pointerInitialPositions.valueAt(0);
        PointF currentPointer0 = pointerPositions.valueAt(0);
        if (pointerPositions.size() == 1) {
            currentState.copyFrom(initialState);
            currentState.moveBy(currentPointer0.x - initialPointer0.x, currentPointer0.y - initialPointer0.y);
            return;
        }

        PointF initialPointer1 = pointerInitialPositions.valueAt(1);
        PointF currentPointer1 = pointerPositions.valueAt(1);

        // Distance between 2 fingers then and now
        float initialDistance = GeometricUtils.getDistance(initialPointer1, initialPointer0);
        float currentDistance = GeometricUtils.getDistance(currentPointer1, currentPointer0);

        // Calculate and apply zoom
        float zoom = currentDistance / initialDistance;
        currentState.setWidth(initialState.getWidth() * zoom);
        currentState.setHeight(initialState.getHeight() * zoom);

        // Angle of initial 2-finger line
        double initialPointersLineAngle = GeometricUtils.getAngle(initialPointer1, initialPointer0);
        // Angle of current 2-finger line
        double currentPointersLineAngle = GeometricUtils.getAngle(currentPointer1, currentPointer0);

        // Angle difference between current and initial 2-finger lines
        // This is how much the sticker was rotated relative to initial state
        double angleDiff = currentPointersLineAngle - initialPointersLineAngle;

        currentState.setAngle((float) (initialState.getAngle() + angleDiff));

        // Calculate corner coordinates of current state
        // Need to know the angle of initial corner relative to initial pointer0.
        // Then add the angle diff to it and find new corner in that direction from current pointer 0.
        float cornerXInitial = initialState.getX();
        float cornerYInitial = initialState.getY();
        // Angle of initial corner relative to pointer0
        double initialCornerAngleWithPointerPivot = GeometricUtils.getAngle(initialPointer0, new PointF(cornerXInitial, cornerYInitial));
        double currentCornerAngleWithPointerPivot = initialCornerAngleWithPointerPivot + angleDiff;

        // Calculate distance of initial corner and initial pointer0
        float initialCornerDistanceX = cornerXInitial - initialPointer0.x;
        float initialCornerDistanceY = cornerYInitial - initialPointer0.y;
        double initialCornerDistance = Math.hypot(initialCornerDistanceX, initialCornerDistanceY);

        float frameX = currentPointer0.x - (float) (initialCornerDistance * zoom * Math.cos(currentCornerAngleWithPointerPivot));
        float frameY = currentPointer0.y - (float) (initialCornerDistance * zoom * Math.sin(currentCornerAngleWithPointerPivot));
        currentState.setX(frameX);
        currentState.setY(frameY);
    }
}