package com.picsart.stickerongifs.model;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * Created by Tigran on 2/2/2017.
 */

public abstract class DrawableObject {

    protected Bitmap bitmap;
    protected Paint imagePaint;

    public DrawableObject(Bitmap bitmap) {
        this.bitmap = bitmap;
        imagePaint = new Paint();
    }

    public abstract void init(float surfaceWidth, float surfaceHeight, float width, float height);

    public abstract FrameComponent getCurrentState();

    public abstract FrameComponent drawOnCanvas(Canvas canvas);
}
