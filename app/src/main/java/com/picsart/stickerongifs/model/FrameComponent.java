package com.picsart.stickerongifs.model;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Tigran on 1/30/2017.
 */

public abstract class FrameComponent implements Parcelable {

    protected String imagePath;
    // Position on canvas
    protected float x;
    protected float y;
    // Size
    protected float width;
    protected float height;

    protected FrameComponent(Parcel source) {
        x = source.readFloat();
        y = source.readFloat();
        imagePath = source.readString();
        width = source.readFloat();
        height = source.readFloat();
    }

    protected FrameComponent(String imagePath) {
        this.imagePath = imagePath;
    }

    protected FrameComponent(FrameComponent source) {
        if (source == null)
            return;
        this.x = source.x;
        this.y = source.y;
        this.imagePath = source.imagePath;
        this.width = source.width;
        this.height = source.height;
    }

    public FrameComponent(String imagePath, float x, float y, float width, float height) {
        this.x = x;
        this.y = y;
        this.imagePath = imagePath;
        this.width = width;
        this.height = height;
    }

    public void copyFrom(FrameComponent component) {
        if (component == null)
            return;
        this.x = component.x;
        this.y = component.y;
        this.imagePath = component.imagePath;
        this.width = component.width;
        this.height = component.height;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getWidth() {
        return width;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public float getHeight() {
        return height;
    }

    public abstract void drawOnCanvas(Canvas canvas, Paint paint);

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeFloat(x);
        dest.writeFloat(y);
        dest.writeString(imagePath);
        dest.writeFloat(width);
        dest.writeFloat(height);
    }
}
