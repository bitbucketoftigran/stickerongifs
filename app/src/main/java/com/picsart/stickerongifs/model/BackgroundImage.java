package com.picsart.stickerongifs.model;

import android.graphics.Bitmap;
import android.graphics.Canvas;

/**
 * Created by Tigran on 2/2/2017.
 */

public class BackgroundImage extends DrawableObject {

    private String filePath;
    private BackgroundComponent currentState;

    public BackgroundImage(Bitmap bitmap, String filePath) {
        super(bitmap);
        this.filePath = filePath;
    }

    @Override
    public void init(float surfaceWidth, float surfaceHeight, float width, float height) {
        //TODO implement "scale type"
        currentState = new BackgroundComponent(filePath, surfaceWidth, surfaceHeight, 255);
    }

    @Override
    public BackgroundComponent getCurrentState() {
        return new BackgroundComponent(currentState);
    }

    public void setAlpha(int alpha) {
        this.currentState.setAlpha(alpha);
    }

    @Override
    public BackgroundComponent drawOnCanvas(Canvas canvas) {
        imagePaint.setAlpha(currentState.getAlpha());
        canvas.drawBitmap(bitmap, currentState.getX(), currentState.getY(), imagePaint);
        return getCurrentState();
    }
}
