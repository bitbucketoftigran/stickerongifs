package com.picsart.stickerongifs.model;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Parcel;

import com.picsart.stickerongifs.utils.ImageLoader;

/**
 * Created by Tigran on 1/30/2017.
 */

public class StickerComponent extends FrameComponent {

    public static final Creator<StickerComponent> CREATOR = new Creator<StickerComponent>() {
        @Override
        public StickerComponent createFromParcel(Parcel in) {
            return new StickerComponent(in);
        }

        @Override
        public StickerComponent[] newArray(int size) {
            return new StickerComponent[size];
        }
    };


    // Rotation angle
    private float angle;

    public StickerComponent(Parcel source) {
        super(source);
        angle = source.readFloat();
    }

    public StickerComponent(StickerComponent source) {
        super(source);
        this.angle = source.angle;
    }

    @Override
    public void copyFrom(FrameComponent component) {
        super.copyFrom(component);
        if (component instanceof StickerComponent)
            this.angle = ((StickerComponent) component).angle;
    }

    public StickerComponent(String imagePath, float x, float y, float width, float height) {
        super(imagePath, x, y, width, height);

    }

    @Override
    public void drawOnCanvas(Canvas canvas, Paint paint) {
        paint.setAlpha(255);
        Bitmap bitmap = ImageLoader.getAssetImageSync(imagePath);
        if (bitmap == null)
            return;
        canvas.rotate((float) Math.toDegrees(angle), x, y);
        RectF rect = new RectF(x, y, x + width, y + height);
        canvas.drawBitmap(bitmap, null, rect, paint);
        canvas.rotate((float) -Math.toDegrees(angle), x, y);
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public void moveBy(float x, float y) {
        this.x += x;
        this.y += y;
    }

    public float getAngle() {
        return angle;
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }

    public boolean contains(float x, float y) {
        double cos = Math.cos(-angle);
        double sin = Math.sin(-angle);
        double newX = this.x + (x - this.x) * cos - (y - this.y) * sin;
        double newY = this.y + (x - this.x) * sin + (y - this.y) * cos;

        return newX > this.x && newX < this.x + width && newY > this.y && newY < this.y + height;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeFloat(angle);
    }
}