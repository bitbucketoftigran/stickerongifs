package com.picsart.stickerongifs.model;

import android.net.Uri;

/**
 * Created by Tigran on 1/27/2017.
 */
public class SelectableImage {

    private Uri sourceUri;
    private int index;

    public SelectableImage(Uri sourceUri) {
        this.sourceUri = sourceUri;
        index = -1;
    }

    public Uri getSourceUri() {
        return sourceUri;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    /**
     * Returns the number to be displayed as selected item's index
     * @return index + 1
     */
    public int getDisplayIndex() {
        return index + 1;
    }
}
