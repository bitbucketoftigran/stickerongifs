package com.picsart.stickerongifs.model;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Tigran on 1/30/2017.
 */

public class Frame implements Parcelable {

    public static final Creator<Frame> CREATOR = new Creator<Frame>() {
        @Override
        public Frame createFromParcel(Parcel in) {
            return new Frame(in);
        }

        @Override
        public Frame[] newArray(int size) {
            return new Frame[size];
        }
    };

    private ArrayList<FrameComponent> components;

    private Frame(Parcel in) {
        components = new ArrayList<>();
        in.readList(components, FrameComponent.class.getClassLoader());
    }

    public Frame() {
        this.components = new ArrayList<>();
    }

    public void addComponent(FrameComponent component) {
        this.components.add(component);
    }

    public synchronized Bitmap render(int width, int height) {
        Bitmap resultBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(resultBitmap);
        Paint paint = new Paint();
        for (FrameComponent component : components)
            component.drawOnCanvas(canvas, paint);
        return resultBitmap;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(components);
    }
}
