package com.picsart.stickerongifs.model;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Parcel;

import com.picsart.stickerongifs.utils.ImageLoader;

import java.io.File;

/**
 * Created by Tigran on 1/30/2017.
 */

public class BackgroundComponent extends FrameComponent {

    public static final Creator<BackgroundComponent> CREATOR = new Creator<BackgroundComponent>() {
        @Override
        public BackgroundComponent createFromParcel(Parcel in) {
            return new BackgroundComponent(in);
        }

        @Override
        public BackgroundComponent[] newArray(int size) {
            return new BackgroundComponent[size];
        }
    };

    private int alpha;

    public BackgroundComponent(Parcel source) {
        super(source);
        alpha = source.readInt();
    }

    public BackgroundComponent(BackgroundComponent source) {
        super(source);
        this.alpha = source.alpha;
    }

    public BackgroundComponent(String imagePath, float width, float height, int alpha) {
        this(imagePath, 0, 0, width, height, alpha);
    }

    public BackgroundComponent(String imagePath, float x, float y, float width, float height, int alpha) {
        super(imagePath, x, y, width, height);
        this.alpha = alpha;
    }

    public void setAlpha(int alpha) {
        this.alpha = alpha;
    }

    public int getAlpha() {
        return alpha;
    }

    @Override
    public void drawOnCanvas(Canvas canvas, Paint paint) {
        Bitmap bitmap = ImageLoader.loadBitmapSync(Uri.fromFile(new File(imagePath)), (int) width, (int) height);
        if (bitmap != null) {
            paint.setAlpha(alpha);
            canvas.drawBitmap(bitmap, x, y, paint);
        }
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeInt(alpha);
    }
}
