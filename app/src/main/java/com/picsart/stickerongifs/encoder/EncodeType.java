package com.picsart.stickerongifs.encoder;

/**
 * Created by Tigran on 2/3/2017.
 */

public enum EncodeType {
    GIF,
    MP4
}
