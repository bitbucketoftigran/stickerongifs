package com.picsart.stickerongifs.encoder;

import android.content.Context;

import com.picsart.stickerongifs.Constants;
import com.picsart.stickerongifs.model.Frame;
import com.waynejo.androidndkgif.GifEncoder;

import java.io.File;
import java.io.IOException;

/**
 * Created by Tigran on 2/2/2017.
 */

public class GifFramesEncoder extends FramesEncoder {

    private GifEncoder gifEncoder;

    public GifFramesEncoder(Context context, String broadcastName) {
        super(context, broadcastName);
    }

    @Override
    protected void beforeEncoding(File outputFile, int frameWidth, int frameHeight) throws IOException {
        gifEncoder = new GifEncoder();
        gifEncoder.init(frameWidth, frameHeight, outputFile.getAbsolutePath(), com.waynejo.androidndkgif.GifEncoder.EncodingType.ENCODING_TYPE_SIMPLE_FAST);
    }

    @Override
    protected void encodeFrame(Frame frame, int frameWidth, int frameHeight) throws IOException {
        gifEncoder.encodeFrame(frame.render(frameWidth, frameHeight), Constants.SAVE_FRAME_TIME);
    }

    @Override
    protected void afterEncoding() throws IOException {
        gifEncoder.close();
    }

    @Override
    protected String getTypeName() {
        return "Gif";
    }
}
