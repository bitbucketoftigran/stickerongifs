package com.picsart.stickerongifs.encoder;

import android.app.IntentService;
import android.content.Intent;
import android.os.Environment;

import com.picsart.stickerongifs.Constants;
import com.picsart.stickerongifs.model.Frame;

import java.io.File;
import java.util.ArrayList;


/**
 * Created by Tigran on 2/3/2017.
 */

public class EncoderIntentService extends IntentService {

    public EncoderIntentService() {
        this("DebugEncoderIntentService");
    }

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public EncoderIntentService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        ArrayList<Frame> frames = intent.getParcelableArrayListExtra("frames");
        int frameWidth = intent.getIntExtra("frame_width", -1);
        int frameHeight = intent.getIntExtra("frame_height", -1);

        String broadcastName = intent.getStringExtra("broadcast_name");
        EncodeType type = (EncodeType) intent.getSerializableExtra("encode_type");

        long timestamp = System.currentTimeMillis();

        File outputFile = null;
        FramesEncoder encoder = null;
        switch (type) {
            case GIF:
                outputFile = new File(Constants.getGifsDirectoryPath(this), timestamp + ".gif");
                encoder = new GifFramesEncoder(this, broadcastName);
                break;
            case MP4:
                outputFile = new File(Constants.getVideosDirectoryPath(this), timestamp + ".mp4");
                encoder = new VideoFramesEncoder(this, broadcastName);
                break;
        }

        encoder.encodeAndSave(frames, frameWidth, frameHeight, outputFile);
    }
}