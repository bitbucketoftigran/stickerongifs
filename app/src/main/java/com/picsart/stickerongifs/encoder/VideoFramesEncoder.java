package com.picsart.stickerongifs.encoder;

import android.content.Context;

import com.picsart.stickerongifs.model.Frame;

import org.jcodec.api.android.SequenceEncoder;

import java.io.File;
import java.io.IOException;

/**
 * Created by Tigran on 2/2/2017.
 */

public class VideoFramesEncoder extends FramesEncoder {

    private SequenceEncoder videoEncoder;

    public VideoFramesEncoder(Context context, String broadcastName) {
        super(context, broadcastName);
    }

    @Override
    protected void beforeEncoding(File outputFile, int frameWidth, int frameHeight) throws IOException {
        videoEncoder = new SequenceEncoder(outputFile);
    }

    @Override
    protected void encodeFrame(Frame frame, int frameWidth, int frameHeight) throws IOException {
        if (videoEncoder != null)
            videoEncoder.encodeImage(frame.render(frameWidth, frameHeight));
    }

    @Override
    protected void afterEncoding() throws IOException {
        if (videoEncoder != null)
            videoEncoder.finish();
    }

    @Override
    protected String getTypeName() {
        return "Video";
    }
}
