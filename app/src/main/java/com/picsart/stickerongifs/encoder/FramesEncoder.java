package com.picsart.stickerongifs.encoder;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;

import com.picsart.stickerongifs.R;
import com.picsart.stickerongifs.model.Frame;
import com.picsart.stickerongifs.utils.Toaster;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Tigran on 2/2/2017.
 */

public abstract class FramesEncoder {
    private Context context;
    private String broadcastName;

    public FramesEncoder(Context context, String broadcastName) {
        this.context = context;
        this.broadcastName = broadcastName;
    }

    public boolean encodeAndSave(final List<Frame> frames, final int frameWidth, final int frameHeight, final File outputFile) {

        if (frames == null || outputFile == null || frameWidth < 0 || frameHeight < 0) {
            return false;
        }

        outputFile.getParentFile().mkdirs();
        if (outputFile.exists())
            outputFile.delete();

        try {
            outputFile.createNewFile();

            beforeEncoding(outputFile, frameWidth, frameHeight);
            int index = 0;
            for (Frame frame : frames) {
                encodeFrame(frame, frameWidth, frameHeight);
                Intent progressIntent = new Intent(broadcastName + "_progress");
                progressIntent.putExtra("frame_index", ++index);
                context.sendBroadcast(progressIntent);
            }
            afterEncoding();
            Intent intent = new Intent(broadcastName + "_finish");
            context.sendBroadcast(intent);
            new Handler(context.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    Toaster.showToast(context.getString(R.string.toast_saved, getTypeName()), Toaster.LENGTH_SHORT);
                }
            });
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    protected abstract void beforeEncoding(File outputFile, int frameWidth, int frameHeight) throws IOException;

    protected abstract void encodeFrame(Frame frame, int frameWidth, int frameHeight) throws IOException;

    protected abstract void afterEncoding() throws IOException;

    protected abstract String getTypeName();
}
